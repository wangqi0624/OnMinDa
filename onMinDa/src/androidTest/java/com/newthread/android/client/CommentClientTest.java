package com.newthread.android.client;

import android.os.SystemClock;
import android.test.ApplicationTestCase;
import android.test.suitebuilder.annotation.LargeTest;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.listener.DeleteListener;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.SaveListener;
import com.newthread.android.activity.main.MyApplication;
import com.newthread.android.bean.remoteBean.StudentUser;
import com.newthread.android.bean.remoteBean.bbs.Comment;
import com.newthread.android.bean.remoteBean.bbs.Post;
import com.newthread.android.manager.BmobRemoteDateManger;
import com.newthread.android.util.Loger;

import java.util.List;

public class CommentClientTest extends ApplicationTestCase<MyApplication> {

    public CommentClientTest() {
        super(MyApplication.class);
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        createApplication();
    }

    /**
     * 对帖子进行回复
     *
     * @throws Exception
     */
    @LargeTest
    public void testAddPost() throws Exception {
        Post post = new Post();
        post.setObjectId("948a80b2e1");
        StudentUser user = new StudentUser(null, null);
        user.setObjectId("5577345bc8");

        Comment comment = new Comment();
        comment.setAuthor(user);
        comment.setContent("老板就是个sb");
        comment.setPost(post);

        BmobRemoteDateManger.getInstance(getApplication()).add(comment, new SaveListener() {
            @Override
            public void onSuccess() {
                Loger.V("保存comment成功");
            }

            @Override
            public void onFailure(int i, String s) {
                Loger.V("保存comment失败" + i + " " + s);
            }
        });

        SystemClock.sleep(4000);
    }

    /**
     * 对评论进行回复
     *
     * @throws Exception
     */
    @LargeTest
    public void testAddComment() throws Exception {
        StudentUser user = new StudentUser(null, null);
        user.setObjectId("60cdcc59f8");

        Comment comment = new Comment();
        comment.setAuthor(user);
        comment.setContent("对他就是个sb");
        Comment commented = new Comment();
        commented.setObjectId("0536462092");
        comment.setComment(commented);

        BmobRemoteDateManger.getInstance(getApplication()).add(comment, new SaveListener() {
            @Override
            public void onSuccess() {
                Loger.V("保存comment成功");
            }

            @Override
            public void onFailure(int i, String s) {
                Loger.V("保存comment失败" + i + " " + s);
            }
        });

        SystemClock.sleep(4000);
    }

    /**
     * 查询自己评论的内容
     *
     * @throws Exception
     */
    @LargeTest
    public void testQuerry() throws Exception {
        final Comment comment = new Comment();
        final StudentUser studentUser = new StudentUser("2012211884", "1994108");
        BmobQuery<StudentUser> query = new BmobQuery<>();
        query.addWhereEqualTo("username", studentUser.getUsername());
        query.addWhereEqualTo("password", studentUser.getPassWord2());
        query.findObjects(getApplication(), new FindListener<StudentUser>() {
            @Override
            public void onSuccess(List<StudentUser> list) {
                comment.setAuthor(list.get(0));
                BmobRemoteDateManger<Comment> bmobRemoteDateManger = BmobRemoteDateManger.getInstance(getApplication());
                bmobRemoteDateManger.querry(comment, new FindListener<Comment>() {
                    @Override
                    public void onSuccess(List<Comment> list) {
                        for (Comment comment : list) {
                            Loger.V(comment);
                        }
                    }

                    @Override
                    public void onError(int i, String s) {
                        Loger.V("error" + i + s);
                    }
                });
            }

            @Override
            public void onError(int i, String s) {

            }
        });


        SystemClock.sleep(4000);
    }

    /**
     * 删除自己的一条评论
     *
     * @throws Exception
     */
    @LargeTest
    public void testDelete() throws Exception {
        Comment comment =new Comment();
        comment.setObjectId("52786e90c1");
        BmobRemoteDateManger.getInstance(getApplication()).delete(comment, new DeleteListener() {
            @Override
            public void onSuccess() {
                Loger.V("删除成功");
            }

            @Override
            public void onFailure(int i, String s) {
                Loger.V("删除失败"+i+s);
            }
        });
        SystemClock.sleep(4000);
    }
}