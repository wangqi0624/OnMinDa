package com.newthread.android.ui.labquery;

public class LoginRequestApi {
	private String Account; // 登陆的账户
	private String Password;// 登陆的密码
    private String QueryURL;//获取网页网址

	public LoginRequestApi(String account, String password,String queryurl) {
		super();
		Account = account;
		Password = password;
        QueryURL = queryurl;
	}

	public String getAccount() {
		return Account;
	}

	public String getPassword() {
		return Password;
	}

    public String getQueryURL() {return QueryURL; }


}
