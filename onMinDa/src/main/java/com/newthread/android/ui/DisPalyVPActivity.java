package com.newthread.android.ui;

import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.newthread.android.R;
import com.newthread.android.activity.main.BaseSFActivity;
import com.newthread.android.util.ImageUtil;
import com.newthread.android.view.ExtendedViewPager;
import com.newthread.android.view.TouchImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * 用于展示图片的viewPager 可放大 缩小 双击放大 滑动等...
 */
public class DisPalyVPActivity extends BaseSFActivity {

    private int selectImagePosition;
    private List<String> images;
    private String loadImageEngine;//imageLoader  picasso

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewpager);
        getSupportActionBar().setTitle("图片展示");
        ExtendedViewPager mViewPager = (ExtendedViewPager) findViewById(R.id.view_pager);
        selectImagePosition = getIntent().getExtras().getInt("selectImagePosition");
        images = getIntent().getExtras().getStringArrayList("images");
        loadImageEngine = getIntent().getExtras().getString("loadImageEngine");
        if (images == null) {
            //未先缓存所有用户数据
            Toast.makeText(getApplicationContext(), "图片为空", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        mViewPager.setAdapter(new TouchImageAdapter());
    }

    class TouchImageAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return images.size();
        }

        @Override
        public View instantiateItem(ViewGroup container, int position) {
            TouchImageView img = new TouchImageView(container.getContext());
            if (loadImageEngine.equals("imageLoader")) {
                ImageUtil.getInstance(getApplicationContext()).disPalyImage(images.get(position), img);
            } else if (loadImageEngine.equals("picasso")) {
                Picasso.with(getApplicationContext())
                        .load(images.get(position))
                        .placeholder(me.nereo.multi_image_selector.R.drawable.default_error)
                        .into(img);
            } else {
                Picasso.with(getApplicationContext())
                        .load(images.get(position))
                        .placeholder(me.nereo.multi_image_selector.R.drawable.default_error)
                        .into(img);
            }

            container.addView(img, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
            return img;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

    }
}
