package com.newthread.android.bean.bbs;

import com.newthread.android.bean.remoteBean.bbs.Post;
import com.newthread.android.bean.remoteBean.bbs.PostImage_1;

import java.util.List;

/**
 * Created by jindongping on 15/6/21.
 */
public class BbsHotItem {

    private Post post;
    private List<PostImage_1> images;

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public List<PostImage_1> getImages() {
        return images;
    }

    public void setImages(List<PostImage_1> images) {
        this.images = images;
    }

    public BbsHotItem(Post post, List<PostImage_1> images) {
        this.post = post;
        this.images = images;
    }

    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BbsHotItem other = (BbsHotItem) obj;
        if (!this.getPost().equals(other.getPost())) {
            return false;
        }
        if (this.getImages() == null && other.getImages() == null)
            return true;
        if (this.getImages() == null) {
            return false;
        }
        if (other.getImages() == null) {
            return false;
        }
        if (this.getImages().size() != other.getImages().size()) {
            return false;
        }
        if (!compareEquals(this.getImages(), other.getImages())) {
            return false;
        }
        return true;
    }

    private boolean compareEquals(List<PostImage_1> images, List<PostImage_1> images1) {
        for (int i=0;i<images.size();i++) {
            if(!images.get(i).equals(images1.get(i))){
                return false;
            }
        }
        return true;
    }
}
