package com.newthread.android.bean.remoteBean.bbs;

import android.content.Context;
import com.newthread.android.bean.remoteBean.RemoteObject;
import com.newthread.android.bean.remoteBean.StudentUser;
import com.newthread.android.client.PostTypeClient;
import com.newthread.android.manager.iMangerService.IRemoteService;

/**
 * Created by jindongping on 15/6/1.
 */
public class PostType extends RemoteObject {

    private String type;//帖子种类名称
    private StudentUser likes;//喜欢改冲类的用户

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public StudentUser getLikes() {
        return likes;
    }

    public void setLikes(StudentUser likes) {
        this.likes = likes;
    }

    @Override
    public String toString() {
        return "PostType{" +
                "id" + getObjectId() +
                "type='" + type + '\'' +
                ", likesIsNull?" + (likes == null) +
                '}';
    }

    @Override
    public IRemoteService getImpl(Context context) {
        return new PostTypeClient(context);
    }
}
