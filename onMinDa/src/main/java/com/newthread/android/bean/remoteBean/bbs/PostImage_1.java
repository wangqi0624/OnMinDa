package com.newthread.android.bean.remoteBean.bbs;

import android.content.Context;
import com.newthread.android.bean.remoteBean.RemoteObject;
import com.newthread.android.client.PostImage_1Client;
import com.newthread.android.manager.iMangerService.IRemoteService;

/**
 * Created by jindongping on 15/6/20.
 */
public class PostImage_1 extends RemoteObject {
    private String imageUrl;
    private String imageName;
    private String imageSize;
    private Post post;//所评论的帖子，这里体现的是一对多的关系，一个图片只能属于一个帖子

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageSize() {
        return imageSize;
    }

    public void setImageSize(String imageSize) {
        this.imageSize = imageSize;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    @Override
    public IRemoteService getImpl(Context context) {
        return new PostImage_1Client(context);
    }

    @Override
    public boolean equals(Object obj) {
        return imageUrl.equals(((PostImage_1) obj).getImageUrl());
    }
}
