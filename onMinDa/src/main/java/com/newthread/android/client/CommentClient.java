package com.newthread.android.client;

import android.content.Context;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.datatype.BmobPointer;
import cn.bmob.v3.listener.DeleteListener;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.SaveListener;
import cn.bmob.v3.listener.UpdateListener;
import com.newthread.android.bean.remoteBean.bbs.Comment;
import com.newthread.android.manager.iMangerService.IRemoteService;
import com.newthread.android.util.Loger;

/**
 * Created by jindongping on 15/5/29.
 */
public class CommentClient implements IRemoteService<Comment> {
    private Context context;

    public CommentClient(Context context) {
        this.context = context;
    }

    @Override
    public void add(Comment comment, SaveListener saveListener) {
        if (comment.getAuthor() == null) {
            Loger.V("评论提交着为空");
            return;
        }
        if (comment.getPost() == null && comment.getComment() == null) {
            Loger.V("评论对象为空");
            return;
        }
        if (comment.getContent() == null) {
            Loger.V("评论内容为空");
            return;
        }
        comment.save(context, saveListener);
    }

    @Override
    public void update(Comment comment, UpdateListener updateListener) {
//        comment.update(context, updateListener);
    }

    /**
     * 根据 评论者查询所有评论
     *
     * @param comment
     * @param findListener
     */
    @Override
    public void querry(Comment comment, FindListener<Comment> findListener) {
        if (comment.getAuthor() == null) {
            Loger.V("评论提交着为空");
            return;
        }
        BmobQuery<Comment> query = new BmobQuery<>();
        query.addWhereEqualTo("author", new BmobPointer(comment.getAuthor()));
        query.findObjects(context, findListener);
    }

    @Override
    public void delete(Comment comment, final DeleteListener deleteListener) {
        comment.delete(context, comment.getObjectId(), deleteListener);
    }
}
