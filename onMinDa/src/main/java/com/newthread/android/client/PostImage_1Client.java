package com.newthread.android.client;

import android.content.Context;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.datatype.BmobPointer;
import cn.bmob.v3.listener.DeleteListener;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.SaveListener;
import cn.bmob.v3.listener.UpdateListener;
import com.newthread.android.bean.remoteBean.bbs.PostImage_1;
import com.newthread.android.manager.iMangerService.IRemoteService;
import com.newthread.android.util.Loger;

/**
 * Created by jindongping on 15/6/20.
 */
public class PostImage_1Client implements IRemoteService<PostImage_1> {
    private Context context;

    public PostImage_1Client(Context context) {
        this.context = context;
    }

    @Override
    public void add(PostImage_1 postImage_1, SaveListener saveListener) {
        if(postImage_1.getPost()==null){
            Loger.V("评论的对象为空");
            return;
        }
        postImage_1.save(context,saveListener);
    }

    @Override
    public void update(PostImage_1 postImage_1, UpdateListener updateListener) {

    }

    @Override
    public void querry(PostImage_1 postImage_1, FindListener<PostImage_1> findListener) {
        if(postImage_1.getPost()==null){
            Loger.V("评论的对象为空");
            return;
        }
        BmobQuery<PostImage_1> query = new BmobQuery<>();
        query.addWhereEqualTo("post",new BmobPointer(postImage_1.getPost()));
        query.findObjects(context, findListener);
    }

    @Override
    public void delete(PostImage_1 postImage_1, DeleteListener deleteListener) {

    }
}
