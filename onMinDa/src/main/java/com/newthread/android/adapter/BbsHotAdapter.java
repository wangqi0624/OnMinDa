package com.newthread.android.adapter;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.*;
import com.bmob.BmobProFile;
import com.newthread.android.R;
import com.newthread.android.activity.main.MyApplication;
import com.newthread.android.bean.bbs.BbsHotItem;
import com.newthread.android.bean.remoteBean.StudentUser;
import com.newthread.android.bean.remoteBean.bbs.PostImage_1;
import com.newthread.android.ui.DisPalyVPActivity;
import com.newthread.android.util.AndroidUtil;
import com.newthread.android.util.ImageUtil;
import com.newthread.android.util.Loger;
import com.newthread.android.view.bbsview.MyTextView;
import com.squareup.picasso.Picasso;
import net.tsz.afinal.FinalActivity;
import net.tsz.afinal.annotation.view.ViewInject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class BbsHotAdapter extends BaseAdapter {
    private Context mContext;
    private List<BbsHotItem> datas;

    private int mItemSize;
    private GridView.LayoutParams mItemLayoutParams = new GridView.LayoutParams(GridView.LayoutParams.MATCH_PARENT, GridView.LayoutParams.MATCH_PARENT);
    ;


    public BbsHotAdapter(Context context, List<BbsHotItem> datas) {
        mContext = context;
        this.datas = datas;
    }


    @Override
    public int getCount() {
        return datas.size();
    }

    @Override
    public BbsHotItem getItem(int position) {
        return datas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.view_bbs_listview_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        final BbsHotItem item = getItem(position);
        StudentUser author = item.getPost().getAuthor();
        //头像
        final String image = BmobProFile.getInstance(mContext).signURL(author.getHeadPhotoName(),
                author.getHeadPhotoUrl(), "430a4eb2231a964487b2fa2c42cff1bb", 0, null);
        ImageUtil.getInstance(mContext).disPalyImage(image, viewHolder.headPhoto);
        viewHolder.headPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> list = new ArrayList<>();
                list.add(image);
                dispalyImg(0, list, "imageLoader");
            }
        });
        viewHolder.name.setHtmlText(author.getName(), 0, author.getName().length(), Color.BLACK);
        viewHolder.title.setText(item.getPost().getTitle());
        viewHolder.content.setText(item.getPost().getContent());
        viewHolder.time.setText(item.getPost().getCreatedAt());
        //展示图片的gridView
        SetGridView(viewHolder.gridView, item);
        //TODO 添加评论
        return convertView;
    }

    private void SetGridView(final GridView gridView, final BbsHotItem item) {
        //1 - 3
        if (0 < item.getImages().size() && item.getImages().size() < 4) {
            //行
            gridView.setColumnWidth(1);
            //列
            gridView.setNumColumns(item.getImages().size());
        }
        //3 - 6
        if (3 < item.getImages().size() && item.getImages().size() < 7) {
            gridView.setColumnWidth(3);
            gridView.setNumColumns(2);
        }
        //大于6
        if (6 < item.getImages().size()) {
            gridView.setColumnWidth(3);
            gridView.setNumColumns(3);
        }
        final GridViewBSAdapter mImageAdapter = new GridViewBSAdapter(item);
        gridView.setAdapter(mImageAdapter);
        gridView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            public void onGlobalLayout() {
                final int width = gridView.getWidth();
                final int desireSize = mContext.getResources().getDimensionPixelOffset(me.nereo.multi_image_selector.R.dimen.image_size);
                final int numCount = width / desireSize;
                final int columnSpace = mContext.getResources().getDimensionPixelOffset(me.nereo.multi_image_selector.R.dimen.space_size);
                int columnWidth = (width - columnSpace * (numCount - 1)) / numCount;
                mImageAdapter.setItemSize(columnWidth);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    gridView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    gridView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
            }
        });
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ArrayList<String> list = new ArrayList<>();
                for (PostImage_1 image : item.getImages()) {
                    String imageReal = BmobProFile.getInstance(mContext).signURL(image.getImageName(),
                            image.getImageUrl(), "430a4eb2231a964487b2fa2c42cff1bb", 0, null);
                    list.add(imageReal);
                }
                dispalyImg(position, list, "picasso");
            }
        });
    }

    private void dispalyImg(int position, ArrayList<String> list, String loadImageEngine) {
        Bundle bundle = new Bundle();
        bundle.putStringArrayList("images", list);
        bundle.putInt("selectImagePosition", position);
        bundle.putString("loadImageEngine", loadImageEngine);
        Intent intent = new Intent(mContext, DisPalyVPActivity.class);
        intent.putExtras(bundle);
        mContext.startActivity(intent);
    }

    private class GridViewBSAdapter extends BaseAdapter {
        private BbsHotItem item;

        public GridViewBSAdapter(BbsHotItem item) {
            this.item = item;
        }

        public void setItemSize(int columnWidth) {
            if (mItemSize == columnWidth) {
                return;
            }
            mItemSize = columnWidth;
            mItemLayoutParams = new GridView.LayoutParams(mItemSize, mItemSize);
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            if (item.getImages().size() > 9) {
                return 9;
            }
            return item.getImages().size();
        }

        @Override
        public PostImage_1 getItem(int position) {
            return item.getImages().get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ImageView imageView;
            PostImage_1 postImage_1 = getItem(position);
            if (convertView == null) {
                imageView = new ImageView(mContext);
                imageView.setLayoutParams(mItemLayoutParams);
            } else {
                imageView = (ImageView) convertView;
            }
            String image = BmobProFile.getInstance(mContext).signURL(postImage_1.getImageName(),
                    postImage_1.getImageUrl(), "430a4eb2231a964487b2fa2c42cff1bb", 0, null);
            if (mItemSize > 0) {
                // 显示图片
                Picasso.with(mContext)
                        .load(image)
                        .placeholder(me.nereo.multi_image_selector.R.drawable.default_error)
                        .resize(mItemSize, mItemSize)
                        .centerCrop()
                        .into(imageView);
            } else {
                //此处32是 R.id.viewpager_bbs_main的16*2
                final int width = AndroidUtil.getScreenWidth((Activity) mContext) - 32;
                final int desireSize = mContext.getResources().getDimensionPixelOffset(me.nereo.multi_image_selector.R.dimen.image_size);
                final int numCount = width / desireSize;
                final int columnSpace = mContext.getResources().getDimensionPixelOffset(me.nereo.multi_image_selector.R.dimen.space_size);
                int columnWidth = (width - columnSpace * (numCount - 1)) / numCount;
                // 显示图片
                Picasso.with(mContext)
                        .load(image)
                        .placeholder(me.nereo.multi_image_selector.R.drawable.default_error)
                        .resize(columnWidth, columnWidth)
                        .centerCrop()
                        .into(imageView);
            }
            GridView.LayoutParams lp = (GridView.LayoutParams) imageView.getLayoutParams();
            if (lp.height != mItemSize) {
                imageView.setLayoutParams(mItemLayoutParams);
            }

            return imageView;
        }
    }

    static class ViewHolder {

        @ViewInject(id = R.id.bbs_hot_headPhoto)
        public ImageView headPhoto;
        @ViewInject(id = R.id.bbs_hot_name)
        public MyTextView name;
        @ViewInject(id = R.id.bbs_hot_time)
        public TextView time;
        @ViewInject(id = R.id.bbs_hot_title)
        public TextView title;
        @ViewInject(id = R.id.bbs_hot_content)
        public TextView content;
        @ViewInject(id = R.id.bbs_hot_gridView)
        public GridView gridView;
        @ViewInject(id = R.id.bbs_hot_linerLayout)
        public LinearLayout linearLayout;

        public ViewHolder(View view) {
            FinalActivity.initInjectedView(this, view);
        }
    }
}
