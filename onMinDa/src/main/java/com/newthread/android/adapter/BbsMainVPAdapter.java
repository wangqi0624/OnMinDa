package com.newthread.android.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.newthread.android.R;
import com.newthread.android.ui.bbs.BbsGroupFrament;
import com.newthread.android.ui.bbs.BbsHistoryFrament;
import com.newthread.android.ui.bbs.BbsHotFrament;
import com.newthread.android.view.JazzyViewPager;
import com.viewpagerindicator.IconPagerAdapter;

/**
 * Created by jindongping on 15/6/4.
 */
public class BbsMainVPAdapter extends FragmentPagerAdapter implements IconPagerAdapter {
    private JazzyViewPager mJazzy;
    private static final int[] ICONS = new int[]{
            R.drawable.skin_tab_icon_conversation_normal,
            R.drawable.skin_tab_icon_contact_normal,
            R.drawable.skin_tab_icon_plugin_normal,
    };

    public BbsMainVPAdapter(FragmentManager fm, JazzyViewPager jazzyViewPager) {
        super(fm);
        this.mJazzy = jazzyViewPager;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new BbsHistoryFrament();
                break;
            case 1:
                fragment = new BbsGroupFrament();
                break;
            case 2:
                fragment = new BbsHotFrament();
                break;
            default:
        }
        mJazzy.setObjectForPosition(fragment, position);
        return fragment;
    }

    @Override
    public int getIconResId(int index) {
        return ICONS[index];
    }

    @Override
    public int getCount() {
        return ICONS.length;
    }
}