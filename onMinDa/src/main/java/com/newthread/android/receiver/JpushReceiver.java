package com.newthread.android.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import cn.jpush.im.android.helpers.IMReceiver;
import com.newthread.android.ui.bbs.BbsChatActivity;

/**
 * Created by jindongping on 15/6/7.
 */
public class JpushReceiver extends BroadcastReceiver {
    IMReceiver imReceiver;

    public JpushReceiver() {
        imReceiver = new IMReceiver();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("cn.jpush.im.android.action.IM_RESPONSE")) {
            imReceiver.onReceive(context, intent);
        } else if (intent.getAction().equals("cn.jpush.im.android.action.NOTIFICATION_CLICK_PROXY")) {
            // 在这里可以自己写代码去定义用户点击后的行为
            Bundle bundle =intent.getExtras();
            Intent i = new Intent(context, BbsChatActivity.class);  //自定义打开的界面
            i.putExtras(intent.getExtras());
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        }
    }
}
