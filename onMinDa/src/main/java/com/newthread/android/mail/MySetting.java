package com.newthread.android.mail;


/**
 * Created by 翌日黄昏 on 13-11-21.
 */
public class MySetting extends AutoMailSetting {

    @Override
    public MailSetting getMailSetting(String address) {
        if (address.endsWith("qq.com")) {
            return getQqMailSetting();
        } else if (address.endsWith("gmail.com")) {
            return getGMailSetting();
        } else if (address.endsWith("126.com")) {
            return get126MailSetting();
        } else if (address.endsWith("163.com")) {
            return get163MailSetting();
        }
        return null;
    }
}
